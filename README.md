**Development Team**

- Ho Wei Bin, Jerry (e0319255@u.nus.edu) - IOT SYSTEM ENGINEER
- Zhang Yilin (e0377000@u.nus.edu) - APP SYSTEM (BACKEND) ENGINEER
- Tan Le Jun (e0406736@u.nus.edu) - APP SYSTEM (FRONTEND) ENGINEER
- Jaedon Kee Shaowei (e0176276@u.nus.edu) - DATABASE SYSTEM ENGINEER

If you face issues with the application please contact the relevant party depending on the issue you face. Also, please CC the other member so your problems could be alerted and rectified as soon as possible.
