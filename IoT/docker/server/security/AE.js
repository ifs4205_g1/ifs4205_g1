// https://www.techengineer.one/how-to-encrypt-decrypt-with-aes-ccm-gcm-in-node-js/
// https://dmyz.org/en/archives/1182
// https://towardsdatascience.com/reading-python-encrypted-data-in-node-js-5b47003dda0
// https://gist.github.com/willshiao/f4b03650e5a82561a460b4a15789cfa1
// https://itnext.io/sharing-aes-256-encrypted-data-between-node-js-and-python-3-d0c87eae212b
// https://www.highgo.ca/2019/08/08/the-difference-in-five-modes-in-the-aes-encryption-algorithm/
// https://lollyrock.com/posts/nodejs-encryption/

ROOT_DIR = '/Users/jerryho/ifs4205_g1/IoT'; //require('path').resolve('./');
const path = require('path');

const NodeRSA = require('node-rsa');
const crypto = require("crypto")
const fs = require("fs");

function print(msg){
    console.log(msg)
}

/* Function which test aes encrypt and decrypt
* Command: // runAESGCMDemo( encryptedVar.algo, encryptedVar.session_key, encryptedVar.nonce, encryptedVar.tag, "Im server");
* */
function runAESGCMDemo(algorithm, key, nonce, tag, plainText) {
    try {

        // The options argument controls stream behavior and is optional except when a cipher in CCM or OCB mode is used (e.g. 'aes-128-ccm').
        // In GCM mode, the authTagLength option is not required but can be used to set the length of the authentication tag that will be returned by getAuthTag() and defaults to 16 bytes.

        var cipher = crypto.createCipheriv(algorithm, key, nonce);
        var decipher = crypto.createDecipheriv(algorithm, key, nonce);

        console.log("\n" + algorithm + ':');

        // Encrypting
        var encText = cipher.update(plainText, 'utf8', 'hex');
        encText += cipher.final('hex');
        console.log("E: " + encText);

        const tag = cipher.getAuthTag();
        print("tag: "); print(tag)
        decipher.setAuthTag(tag);

        // Decrypting
        var decText = decipher.update(encText, 'hex', 'utf8');
        decText += decipher.final('utf8');
        console.log("D: " + decText);

        console.log("MATCH: " + (decText === plainText));

    } catch (e) {

        console.log(e);
    }
}

function hexStringToByteArray(hexString) {
    if (hexString.length % 2 !== 0) {
        throw "Must have an even number of hex digits to convert to bytes";
    }/* w w w.  jav  a2 s .  c o  m*/
    var numBytes = hexString.length / 2;
    var byteArray = new Uint8Array(numBytes);
    for (var i=0; i<numBytes; i++) {
        byteArray[i] = parseInt(hexString.substr(i*2, 2), 16);
    }
    return byteArray;
}

// Convert a hex string to a byte array
function hexToBytes(hex) {
    for (var bytes = [], c = 0; c < hex.length; c += 2)
    bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
}

function bufferToHex (buffer) {
            return [...new Uint8Array (buffer)]
                .map (b => b.toString (16).padStart (2, "0"))
                .join ("");
}
/* runEncryptAESGCM("id-aes256-gcm", enc_session_key, encrypt_nonce, message, signed_message) */
function runEncryptAESGCM(algorithm, key, nonce, plainText, signed_message) {
    try {
        console.log("\n********************\n" + "Encrypt - " + algorithm + ':');


        let cipher = crypto.createCipheriv(algorithm, key, nonce);
        let str_signed_msg = bufferToHex(signed_message)

        // console.log("\n"+ "Encrypt - " + algorithm + ':');
        let encrypt_obj = JSON.stringify({"msg": plainText, "signed_msg": str_signed_msg}); //plainText;//

        // Encrypting
        let encText = cipher.update(encrypt_obj, 'utf8', 'hex');
        // let encrypted = Buffer.concat([encText, cipher.final()]);

        encText += cipher.final('hex');

        let aesVar = {enc_session_key: key, aes_nonce: nonce, aes_tag: cipher.getAuthTag(), cipherText: Buffer.from(encText)};
        console.log("Encryption completed" + "\n********************\n");
        // print("E: ");
        // print(aesVar);

        return aesVar;
    } catch (e) {
        console.log(e);
    }
}

function runDecryptAESGCM(algorithm, key, nonce, tag, plainText, cipherText) {
    try {
        // The options argument controls stream behavior and is optional except when a cipher in CCM or OCB mode is used (e.g. 'aes-128-ccm').
        // In GCM mode, the authTagLength option is not required but can be used to set the length of the authentication tag that will be returned by getAuthTag() and defaults to 16 bytes.
        var decipher = crypto.createDecipheriv(algorithm, key, nonce);

        console.log("\n********************\n" + "Decrypt - " + algorithm + ':\n');

        decipher.setAuthTag(tag);

        // Decrypting
        let decText = decipher.update(cipherText, 'utf-8', 'utf8');
        decText += decipher.final('utf8');
        // console.log("D: " + decText);

        const DecryptedObj= JSON.parse(decText)


        print("The signature is valid. (AES)");
        return DecryptedObj;
    } catch (e) {
        print("The signature is not valid. (AES)");
        console.log(e);
    }
}

function write_to_file_aes(obj, e_session_key) {
    const callback = (err) => {
      if (err) throw err;
      // console.log('It\'s saved!');
    };

    fs.writeFile('/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/client/aes_encrypt_enc_session_key.txt', e_session_key, {}, callback);
    fs.writeFile('/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/client/aes_encrypt_nonce.txt', obj.aes_nonce, {}, callback);
    fs.writeFile('/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/client/aes_encrypt_tag.txt', obj.aes_tag, {}, callback);
    fs.writeFile('/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/client/aes_encrypt_cipher.txt', obj.cipherText, {}, callback);
}

/**
 * This function is used when the pc request for hash.
 ***/
function encrypt_aes_and_rsa(encrypt_message, encrypt_nonce, encrypt_session_key, my_privateKey, client_publicKey) {
    /* Encrypt and send to client */
    const encrypt_signed_message = new NodeRSA(my_privateKey).sign(Buffer.from(encrypt_message),"buffer", "buffer");
    const encrypt_enc_session_key = new NodeRSA(client_publicKey).encrypt(encrypt_session_key, "buffer", "buffer");

    let aes_encrypt_obj = runEncryptAESGCM("id-aes256-gcm", encrypt_session_key, encrypt_nonce, encrypt_message, encrypt_signed_message)

    // write_to_file_aes(aes_encrypt_obj, encrypt_enc_session_key)
    return {aes_obj: aes_encrypt_obj, rsa_session_key: encrypt_enc_session_key}
}

/**
 * This function is used when the pc send logs.
 ***/
function decrypt_aes_and_rsa(enc_session_key, aes_nonce, aes_tag, aes_ciphertext, client_publicKey, my_privateKey) {
    const aes_session_key = crypto.privateDecrypt(my_privateKey, enc_session_key)

    const encryptedVar = {algo: "id-aes256-gcm", nonce: aes_nonce, tag: aes_tag, ciphertext: aes_ciphertext, session_key: aes_session_key}

   /* Decrypt From server send */
    const DecryptedObj = runDecryptAESGCM( encryptedVar.algo, encryptedVar.session_key, encryptedVar.nonce, encryptedVar.tag, "Im server", encryptedVar.ciphertext);



    const check_signed_msg = new NodeRSA(client_publicKey).verify(Buffer.from(DecryptedObj["msg"]), DecryptedObj["signed_msg"],
            'buffer', 'hex')
    const message = DecryptedObj["msg"];
    if (check_signed_msg) {
        console.log("The signature is valid. (SIGNED_MSG)");
        console.log(message);
        console.log("\nDecryption completed" + "\n********************\n");
        return {"verified": true, "message": message}
    }
    else {
        console.log("The signature is not valid (SIGNED_MSG)");
        console.log(message);
        console.log("\nDecryption completed" + "\n********************\n");
        return {"verified": false, "message": "error"}//"error"
    }

}


//Read Private_key and Public_key
// const c_privateKey = fs.readFileSync("/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/crypto_pc1_private.pem");
// const c_publicKey = fs.readFileSync("/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/crypto_pc1_public.pem");
//
// const s_publicKey = fs.readFileSync("/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/crypto_server_public.pem");
// const s_privateKey = fs.readFileSync("/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/crypto_server_private.pem");


/* Generate AES variable */
// const encrypt_message = '5807E3CB-2D54-46C3-8007-5C338862F387';
// const encrypt_nonce = crypto.randomBytes(16);
// const encrypt_session_key = crypto.randomBytes(32);
// encrypt_aes_and_rsa(encrypt_message, encrypt_nonce, encrypt_session_key, s_privateKey, c_publicKey);

/* Read server AES data */
// const enc_session_key = hexStringToByteArray(fs.readFileSync("/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/server/aes_encrypt_enc_session_key.txt").toString());
// const aes_nonce = hexStringToByteArray(fs.readFileSync("/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/server/aes_encrypt_nonce.txt").toString());
// const aes_tag = hexStringToByteArray(fs.readFileSync("/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/server/aes_encrypt_tag.txt").toString());
// const aes_ciphertext =  hexStringToByteArray(fs.readFileSync("/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/server/aes_encrypt_cipher.txt").toString());

// decrypt_aes_and_rsa(enc_session_key, aes_nonce, aes_tag, aes_ciphertext, c_publicKey, s_privateKey);


module.exports = {
    encrypt_aes_and_rsa,
    decrypt_aes_and_rsa,
    bufferToHex
};

