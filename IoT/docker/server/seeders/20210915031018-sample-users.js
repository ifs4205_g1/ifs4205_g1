'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return await queryInterface.bulkInsert(
        "Users",
        [
          {
            user: "demouser",
            ble_id: "0B7965FA-AC1F-492D-A3EC-D574CD8C1233",
            client_hash: "6bf57dc6b340317a05682e3df2a2f01f4da74a56dd4d50ea10ea502a0dca92c4",
            server_hash: "d6c6e3f33532b71144e0d837982a3ef0c35eb06f7301d9dfe70afb8386e17ffc",
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            user: "demoofficial",
            ble_id: "5807E3CB-2D54-46C3-8007-5C338862F387",
            client_hash: "7eca4d42e5a3289e0bcfb2dd340197f17b017cf0d43cd006d19e77525e664458",
            server_hash: "c63a98e412751cae8176f13625b3af1606d91f8594d583b557b1eba4a7a5579d",
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            user: "3",
            ble_id: "hij",
            client_hash: "ch",
            server_hash: "sh",
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            user: "4",
            ble_id: "klm",
            client_hash: "ch",
            server_hash: "sh",
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            user: "5",
            ble_id: "nop",
            client_hash: "ch",
            server_hash: "sh",
            createdAt: new Date(),
            updatedAt: new Date()
          }
        ],
        {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return await queryInterface.bulkDelete("Users", null, {});
  }
};
