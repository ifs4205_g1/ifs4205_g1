const config = require("../config/config.js")

const {Client, Pool} = require("pg");

const dbName = config.development.database;

const pool = new Pool({
    port: process.env.DB_PORT,
    host: config.development.host,
    user: config.development.username,
    password: config.development.password
});

const query_drop = `DROP DATABASE IF EXISTS ${dbName};`
const query_create = `CREATE DATABASE  ${dbName};`


pool.connect(async (err, client, release) => {
  if (err) {
    return console.error('Error acquiring client', err.stack)
  }
  await client.query(query_drop, (err) => {
    if (err) {
      return console.error('Error executing query', err.stack)
    }
    console.log("Database dropped")
  })

  await client.query(query_create, (err) => {
    release()
    if (err) {
      return console.error('Error executing query', err.stack)
    }
    console.log("Database created")
  })
})

pool.end().then(() => console.log('pool has ended'))

/*
client.connect().then(() => {
    client.query(query_drop, (err, res) => {
        if (err) throw err
        console.info("Database create or successfully checked");
        client.end()
    })
})

client.connect().then(() => {
    client.query(query_create, (err, res) => {
        if (err) throw err
        console.info("Database create or successfully checked");
        client.end()
    })
})
*/