// Reference: https://www.youtube.com/watch?v=-MTSQjw5DrM
//https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
//https://semaphoreci.com/community/tutorials/dockerizing-a-node-js-web-application
'use strict';

require('dotenv').config();
// const { DB_HOST, DB_USERNAME, DB_PASSWORD } = process.env;
const models = require('./models')
const express = require('express')
const {Op, STRING} = require("sequelize");
const app = express();
const hostname = 'http://localhost'
const port = process.env.PORT || 8080

const { Pool, Client } = require('pg')
// const config = require("home/app/config/config.js");

//Main database connection

const client = new Client({
    user: process.env.MOTHER_DB_USER,
    host: process.env.MOTHER_DB_HOST,
    database: process.env.MOTHER_DB_DATABASE,
    password: process.env.MOTHER_DB_PASSWORD,
    port: process.env.MOTHER_DB_PORT,
});

//"postgres://postgres:password@172.25.76.254:5432/sequelize_database_dev"

// const pgPoolWrapper = {
// async connect() {
//     for (let nRetry = 1; ; nRetry++) {
//         try {
//             const client = await pgPool.connect();
//             if (nRetry > 1) {
//                 console.info('Now successfully connected to Postgres');
//             }
//             return client;
//         } catch (e) {
//             if (e.toString().includes('ECONNREFUSED') && nRetry < 5) {
//                 console.info('ECONNREFUSED connecting to Postgres, ' +
//                     'maybe container is not ready yet, will retry ' + nRetry);
//                 // Wait 1 second
//                 await new Promise(resolve => setTimeout(resolve, 1000));
//             } else {
//                 throw e;
//             }
//         }
//     }
// }
// };
    // {
    // port: 5432 || 3000,
    // host: '172.25.76.254',
    // user: 'postgres',
    // password: 'password',
    // database: 'sequelize_database_dev'
    // user: process.env.MOTHER_DB_USER,
    // host: process.env.MOTHER_DB_HOST,
    // database: process.env.MOTHER_DB_DATABASE,
    // password: process.env.MOTHER_DB_PASSWORD,
    // port: process.env.MOTHER_DB_PORT,
// }


app.use(express.json())

//Broadcast server listening
app.listen(
    port,
    () => console.log(`Server is alive on ${hostname}:${port}`)
)

// FUNCTIONALITIES
/**
* This function retrieves the server_hash given the service_id.
***/
app.get('/hash/', async (req, res) =>{
    const { ble_id } = req.body;
    console.log(JSON.stringify(req.headers))
    console.log(JSON.stringify(req.body))
    console.log(JSON.stringify(req.params))

    if(!ble_id) {
        res.status(418).send({ message: 'We need a ble_id'})
    }

     try {
        const userid = await models.User.findAll({
            where: {
                ble_id: ble_id
            }
        })
        if (!userid.length) {
            res.send({message: ` No user records found`})
        } else {
            const sh = userid[0].dataValues.server_hash;
            res.status(200).send({
                message: `User ${ble_id} with ${sh} found`,
                ble_id: ble_id,
                sh: sh
            })
        }
    } catch (error) {
        console.error(error)
    }

})

/**
* This function creates a new user to the IoT user database.
* This behavior is only allowed from Application Server.
**/
app.post('/adduser/:user', async (req, res) => {
    const {user} = req.params;
    const {ble_id} = req.body;

    if (!ble_id) {
        res.status(418).send({message: 'We need a ble_id'})
    }

    try {
        const userid = await models.User.findAll({
            where: {
                [Op.or]:[
                    {user: user}, {ble_id: ble_id}
                ]
            }
        })

        if(!userid.length) {
            models.User.create({user: user, ble_id: ble_id, client_hash: "ch", server_hash: "sh"})
            res.status(200).send({
                message: `User ${user} added`
            })
        } else {
            res.send(`User records found`)
        }
    } catch (error) {
        console.error(error)
    }
})

/**
* This function uploads activity log to the IoT Log database
***/
app.post('/upload/:pc', async (req,res) => {
    const pc = req.params.pc
    const activities = req.body

    if(!(Object.keys(activities).length)) {
        res.status(418).send({ message: 'We need a activities'})
    }
    console.log(activities)

    try {

        for (const activity in Object.keys(activities)) {
            console.log("Activity");
            console.log(activity)
            console.log(activities[activity])
            //Upload data into Local Database
            models.Log.create({pc: pc, ble_id: activities[activity]["ble_id"], timestamp: Date.parse(activities[activity]["timestamp"])})
        }

        res.status(200).send({message: `Activity log added`})
    } catch (error) {
       console.error(error)
    }
    for (const activity in Object.keys(activities)) {
        try {
            const ble_id = await models.User.findAll({
                 where: {
                     ble_id: activities[activity]["ble_id"]
                 }
             })


            if (ble_id.length) {
                const user_id = ble_id[0].dataValues.user;

                //Update data to Main
                const query = `insert into iot_log(idnum,time) values ('${user_id}', '${activities[activity]["timestamp"]}');`
                console.log(query)
                await client.connect();
                await client.query(query, async (err, res) => {
                    console.log(err, res)
                    await client.end();
                })


            } else {
                console.log("NO USER")
            }
        } catch (error) {
            console.error(error)
        }
    }


})


/**
 * These functions below are for testing purposes
 ***/
app.get('/user/:user', async (req, res) => {
    const user = req.params.user
    try {
        const userid = await models.User.findAll({
            where: {
                user: user
            }
        }
    )
    if(userid.length) {
        const ble_id = userid[0].dataValues.ble_id;
        res.status(200).send({
            message: `User ${user} with ble_id ${ble_id}`
        })
    } else {
        res.send({
            message: `No user in records`
        })
    }
    } catch(error) {
        console.error(error)
    }
})

app.post('/hash/:user', (req, res) => {
    const { user } = req.params;
    const { service_id } = req.body;

    if(!service_id) {
        res.status(418).send({ message: 'We need a service_id'})
    }

    res.status(200).send({
        data: `Request from ${user} with  service_id: ${service_id} `,
        client_hash: ``,
        server_hash: ``,
    })
})

app.get('/test', (req, res) => {
    res.status(200).send(
        {message:"hello"}
    )
})

//Test connection get
app.get('/', (req, res) => res.status(200).send('hello'));