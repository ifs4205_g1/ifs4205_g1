# https://pycryptodome.readthedocs.io/en/latest/src/examples.html
# https://pycryptodome.readthedocs.io/en/latest/src/signature/pkcs1_v1_5.html#Crypto.Signature.pkcs1_15.PKCS115_SigScheme
# https://pycryptodome.readthedocs.io/en/latest/src/public_key/rsa.html
# https://discuss.python.org/t/how-to-use-rsa-public-key-to-decrypt-ciphertext-in-python/2919/4
# https://github.com/anik123/RSA-encryption-in-Python-and-decryption-in-NodeJs/blob/master/Encryption.py
# https://titanwolf.org/Network/Articles/Article?AID=7211ded3-94dd-499d-b38b-0974435061ba#gsc.tab=0
# https://wizardforcel.gitbooks.io/practical-cryptography-for-developers-book/content/digital-signatures/rsa-signatures.html

import base64
import binascii
import codecs
import json
import os

from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15
from Crypto.Cipher import PKCS1_OAEP, AES
from Crypto.Random import get_random_bytes

ROOT_DIR = '/Users/jerryho/ifs4205_g1/IoT'

def _pad(s): return s + (AES.block_size - len(s) % AES.block_size) * chr(AES.block_size - len(s) % AES.block_size)


def read_from_file_public(filename, encryption):
    directory = os.path.join(ROOT_DIR, 'docker/server/pki/store')
    with open(f'{directory}/{encryption}_{filename}_private.pem', "rb") as f:
        pri = RSA.importKey(f.read())
    with open(f'{directory}/{encryption}_{filename}_public.pem', "rb") as f:
        pub = RSA.importKey(f.read())
    return {"private": pri, "public": pub}


def sign_message(text, private_key):
    h = SHA256.new(text.encode())
    signature = pkcs1_15.new(private_key).sign(h)
    return signature


def verify_sign_message(signature, public_key, msg):
    h = SHA256.new(msg)

    try:
        pkcs1_15.new(public_key).verify(h, signature)
        print("The signature is valid. (SIGNED_MSG)")
        return True
    except (ValueError, TypeError):
        print("The signature is not valid. (SIGNED_MSG)")
        return False


def write_to_file(filename, data):
    directory = os.path.join(ROOT_DIR, 'docker/server/pki/store/server')
    with open(f'{directory}/{filename}.txt', "wb") as f:
        f.write(data)


def read_from_file(filename):
    directory = os.path.join(ROOT_DIR, 'docker/server/pki/store/client')
    with open(f'{directory}/aes_encrypt_{filename}.txt', "rb") as f:
        content = f.read()
    return content


def encrypt(public_key, encoded_plaintext):
    encryptor = PKCS1_OAEP.new(public_key)
    encrypted = encryptor.encrypt(encoded_plaintext)
    return encrypted


def decrypt(private_key, encoded_ciphertext):
    decryptor = PKCS1_OAEP.new(private_key)
    decrypted = decryptor.decrypt(encoded_ciphertext)

    return decrypted


def write_to_file_aes(filename, rsa_enc_session_key, aes_cipher_nonce, aes_ciphertext, aes_tag):
    write_to_file(filename=f'{filename}_tag', data=aes_tag)
    write_to_file(filename=f'{filename}_nonce', data=aes_cipher_nonce)
    write_to_file(filename=f'{filename}_enc_session_key', data=rsa_enc_session_key)
    write_to_file(filename=f'{filename}_cipher', data=aes_ciphertext)


def aes_encrypt(algo, session_key, msg, signed_msg):
    cipher_aes = AES.new(session_key, algo)
    ciphertext, tag = cipher_aes.encrypt_and_digest(
        (json.dumps({"msg": msg, "signed_msg": signed_msg.hex()})).encode())

    return {"ciphertext": ciphertext, "tag": tag, "nonce": cipher_aes.nonce}


def aes_decrypt(algo, session_key, nonce, tag, ciphertext):
    decipher_aes = AES.new(key=session_key, mode=algo, nonce=nonce)
    try:
        decrypted_obj_str = decipher_aes.decrypt_and_verify(bytes.fromhex(ciphertext.decode()), tag)
        # print(decrypted_obj_str)
        decrypted_obj = json.loads(decrypted_obj_str.decode())

        return decrypted_obj
    except ValueError:
        return {"error": "error"}


# -----------------------------------------------------------
# This function encrypt logs for sending them to the server
# -----------------------------------------------------------
def encrypt_aes_and_rsa(algo, key, msg, my_private, server_public):
    print('********************\nEncrypt - id-aes256-gcm:')
    signed_msg = sign_message(text=msg, private_key=my_private)

    # Encrypt the session key with the public RSA key
    enc_session_key = encrypt(public_key=server_public, encoded_plaintext=key)

    # Encrypt the data(message and signed message) with the AES session key

    aes_encrypt_obj = aes_encrypt(algo=algo, session_key=key, msg=msg, signed_msg=signed_msg)
    ciphertext, tag, nonce = aes_encrypt_obj["ciphertext"], aes_encrypt_obj["tag"], aes_encrypt_obj["nonce"]



    write_to_file_aes(filename="aes_encrypt",
                      rsa_enc_session_key=enc_session_key.hex().encode(),
                      aes_cipher_nonce=nonce.hex().encode(),
                      aes_ciphertext=ciphertext.hex().encode(),
                      aes_tag=tag.hex().encode())

    print('Encryption completed\n********************')
    return {"enc_session_key": enc_session_key, "nonce": nonce, "tag": tag, "cipher": ciphertext}


# -----------------------------------------------------------
# This function decrypt hash received from to the server
# -----------------------------------------------------------
def decrypt_aes_and_rsa(enc_session_key, nonce, tag, cipher, my_private, server_public):
    print('********************\nDecrypt - id-aes256-gcm:\n')
    dec_session_key = decrypt(private_key=my_private, encoded_ciphertext=enc_session_key)

    decrypted_obj = aes_decrypt(AES.MODE_GCM, dec_session_key, nonce, tag, cipher)
    if "error" not in decrypted_obj:
        print("The signature is valid. (AES)")
        verify_sign_message(bytearray.fromhex(decrypted_obj["signed_msg"]), server_public, decrypted_obj["msg"].encode())
        msg = decrypted_obj["msg"]
        print(msg)
        print('\nDecryption completed\n********************')
        return {"verified": True, "message": msg}

    print('\nDecryption completed\n********************')
    print("The signature is not valid. (AES)")
    return {"verified": False, "message": "error"}


# server = read_from_file_public(filename="server", encryption="crypto")
# pc1 = read_from_file_public(filename="pc1", encryption="crypto")

# Generate sign of message
# x = {"2": {"ble_id": "5807E3CB-2D54-46C3-8007-5C338862F387", "timestamp": "2021-10-14 09:21:56.438888"}}
# message = json.dumps(x)
# session_key = get_random_bytes(32)

# encrypt_aes_and_rsa(algo=AES.MODE_GCM, key=session_key, msg=message, my_private=pc1["private"], server_public=server["public"])

# enc_session_key = read_from_file("enc_session_key")
# nonce = read_from_file("nonce")
# tag = read_from_file("tag")
# cipher = read_from_file("cipher")
# decrypt_aes_and_rsa(enc_session_key, nonce, tag, cipher, my_private=pc1["private"], server_public=server["public"])
