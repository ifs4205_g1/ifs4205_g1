# https://realpython.com/python-https/
# https://www.nylas.com/blog/use-python-requests-module-rest-apis/
import json

import requests
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from requests.adapters import HTTPAdapter
from requests.exceptions import ConnectionError

# Connect to a GMS echo service
from adafruit_ble import BLERadio
from adafruit_ble.advertising.standard import ProvideServicesAdvertisement
from adafruit_ble.services.gmsservice import GMS

import base64
import datetime
import hashlib
import hmac
import os
import string
from dotenv import load_dotenv

load_dotenv()

from security import AE


def read_from_file_public(filename, encryption):
    with open(f'/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/{encryption}_{filename}_private.pem', "rb") as f:
        pri = RSA.importKey(f.read())
    with open(f'/Users/jerryho/ifs4205_g1/IoT/docker/server/pki/store/{encryption}_{filename}_public.pem', "rb") as f:
        pub = RSA.importKey(f.read())
    return {"private": pri, "public": pub}


# Environment Variables to be setup later
PORT = os.environ.get('PORT')
PC = os.environ.get('PC')
HOST = os.environ.get('HOST')

server = read_from_file_public(filename="server", encryption="crypto")
pc1 = read_from_file_public(filename="pc1", encryption="crypto")


# These functions interact with the iot server
def setup_connection(subdirectory):
    server_adapter = HTTPAdapter(max_retries=0)
    session = requests.Session()
    session.mount(HOST + ':' + PORT + '/', server_adapter)

    return session


def upload_log(logs, session):
    res = {}
    try:
        res = session.post(HOST + ':' + PORT + '/upload/' + PC, json=logs, )
    except ConnectionError as ce:
        print(ce)

    return res


def get_hash(ble_id: str):
    res = {}
    try:
        res = requests.get(HOST + ':' + PORT + '/hash', json={'ble_id': ble_id}, )
    except ConnectionError as ce:
        print(ce)

    return res


# This function performs xor of two hex strings
def xor(str1, str2):
    return hex(int(str1, 16) ^ int(str2, 16))[2:]


def checkifhexstring(str):
    return all(c in string.hexdigits for c in str)


ble = BLERadio()

found = dict()
log = dict()

GMS_connection = None

if not GMS_connection:
    print("Trying to connect...")
    for adv in ble.start_scan(ProvideServicesAdvertisement):
        dev_addr = adv.address.string
        if dev_addr in found:  # For now only can enter. TBC because user can enter and exit
            if found[dev_addr][2] == True and (
                    datetime.datetime.now() - found[dev_addr][1]).total_seconds() < 5:  # buffer 05seconds
                print("Hello connect later")
                continue
        # if dev_addr not in found: Update regardless since new time need to be updated
        found[dev_addr] = (adv.complete_name, datetime.datetime.now(), True if GMS in adv.services else False)

        if GMS in adv.services:
            GMS_connection = ble.connect(adv)
            print("Connected: " + dev_addr + " | " + str(datetime.datetime.now()))

            if GMS_connection and GMS_connection.connected:
                GMS_service = GMS_connection[GMS]

                receive = ""

                while (str(receive) == str("")) or checkifhexstring(receive) == False:
                    # Initial Challenge and Response & Query IoT Server for server_hash
                    challenge = os.urandom(32)  # bcrypt.gensalt(16)
                    challenge_b64 = base64.b64encode(challenge).decode("ascii")

                    # print(dev_addr, adv.complete_name, adv)
                    # print("\t" + repr(adv))
                    GMS_service.write(challenge_b64.encode())

                    # Query IoT Server
                    response = get_hash(dev_addr)  # , setup_connection('hash')
                    # print(response)
                    # print(response.json())
                    decoded_aes = AE.decrypt_aes_and_rsa(bytearray.fromhex(response.json()["enc_session_key"]),
                                                         bytearray.fromhex(response.json()["nonce"]),
                                                         bytearray.fromhex(response.json()["tag"]),
                                                         bytearray.fromhex(response.json()["cipher"]),
                                                         my_private=pc1["private"], server_public=server["public"])

                    if decoded_aes["verified"]:
                        sh = decoded_aes["message"]
                    else:
                        sh = decoded_aes["message"]
                    hash_sh_n_c = hmac.new(key=challenge_b64.encode(), msg=sh.encode(), digestmod=hashlib.sha256)
                    # use a do while loop if receive is corrupted
                    receive = ""
                    while (temp := GMS_service.read()) is not None:
                        receive += temp.decode("utf-8")
                    print("Message Receive: [" + receive.lower() + "]")
                    # print(receive)

                    # if not checkifhexstring(receive):
                    #     print("ERROR FOUND")
                    # else:
                    #     print("GOOD")

                R = receive.lower()
                RR = hashlib.sha256(xor(hash_sh_n_c.hexdigest(), R).encode()).hexdigest()

                if RR == sh:
                    print("Challenge and Response: MATCHED")
                    log[str(len(log))] = {"ble_id": dev_addr, "timestamp": str(found[dev_addr][1])}
                    print("Log entry: ", end="")
                    print(log)

                    session_key = get_random_bytes(32)
                    m = json.dumps(log)

                    aes_encoded_obj = AE.encrypt_aes_and_rsa(algo=AES.MODE_GCM,
                                                             key=session_key,
                                                             msg=m,
                                                             my_private=pc1["private"], server_public=server["public"])
                    hex_aes_encoded_obj = {"enc_session_key": aes_encoded_obj["enc_session_key"].hex(),
                                           "nonce": aes_encoded_obj["nonce"].hex(),
                                           "tag": aes_encoded_obj["tag"].hex(),
                                           "cipher": aes_encoded_obj["cipher"].hex()}
                    if log is not None:
                        response = upload_log(hex_aes_encoded_obj, setup_connection('upload'))
                        print(response)
                        print(response.json()["message"])

                        if response.json()["message"] == 'Activity log added':
                            log = dict()
                else:
                    print("Challenge and Response: NOT MATCHED")

                    print(RR)
                    print(sh)

                GMS_connection.disconnect()
                print("Disconnected: ", end="")
                print(dev_addr)
        print("Other devices?")
    ble.stop_scan()
