/*
  The sketch demonstrates the communication between the IoT PC and a User dongle.
  Primary connection, will be initated by the IoT PC. 
  The dongle will be listening within the radius of the BLE settings (-20dbm).
  Secondary connection, will be initated by the User dongle which broadcasts its service address
  Third connection, will be a challenge issued by the IoT PC.
  Fourth connection, will be a response by the dongle.

  Disconnect thereafter.

*/
//#include <valarray>
#include <stdio.h>
#include <string.h>
#include <RFduinoBLE.h>
#include <Crypto.h>
#include <BLAKE2b.h>
#include <SHA256.h>

#define HASH_SIZE 32
#define CHALLENGE_SIZE 44
#define looper(x,n) for(int x = 0; x < n; ++x) Serial.print("HELLO");
//#define f(a,b) for(int x=0;*a+*b;b+=!!*b)a+=putchar(*a^*b)!=*b;

struct TestSHA256Vector
{
    const char *name;
    const char *client_data; //h(password, salt)
    const char *server_data; //h(h(password, salt)) #salt is case sensitive
    uint8_t *resultsha[64]; // store hash
};

static TestSHA256Vector const testvector = {
  "SHA256",
  "6bf57dc6b340317a05682e3df2a2f01f4da74a56dd4d50ea10ea502a0dca92c4",
  "d6c6e3f33532b71144e0d837982a3ef0c35eb06f7301d9dfe70afb8386e17ffc"
  };

bool rssidisplay;

const char * myid = "BLE_U2";

const char* quads[] = { "0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111" };

const char * chrTObin(unsigned char c) {
  if(c >= '0' && c <= '9') return quads[     c - '0'];
  if(c >= 'A' && c <= 'F') return quads[10 + c - 'A'];
  if(c >= 'a' && c <= 'f') return quads[10 + c - 'a'];
  return NULL;
  //return -1;
}

#define FALSE 0
#define TRUE !(FALSE)

int checkIFHex(unsigned char c) {
  if(c >= '0' && c <= '9') return TRUE;
  if(c >= 'A' && c <= 'F') return TRUE;
  if(c >= 'a' && c <= 'f') return TRUE;
  return FALSE;
}

const int checkIFHexString(char * str) {
  int len = strlen(str);

  for(int i = 0; i < len; i++) {
    if(checkIFHex(str[i]) == FALSE)
      return FALSE;
  }
  return TRUE;
}

char *xorHash(char chrXOR1, char chrXOR2) {

    int x;
    int xPos;


    char strBin1[4];
    char strBin2[4];
    char strBin[8];
    char strXORED[4];
    char newXOR[128];

    strcpy(newXOR, "");

    strcpy(strBin, "");
    strcat(strBin, chrTObin(chrXOR1));
    strcat(strBin, chrTObin(chrXOR2));

    strcpy(strXORED, "");

    if(strlen(strBin) == 8) {
        for(xPos = 0; xPos < 4; xPos++) {
            if(strBin[xPos] == strBin[xPos+4]) {
                strcat(strXORED, "0");
            } else {
                strcat(strXORED, "1");
            }
        }
    }

    if(strcmp(strXORED, "0000") == 0) {
        strcat(newXOR, "0");
    } else if(strcmp(strXORED, "0001") == 0) {
        strcat(newXOR, "1");
    } else if(strcmp(strXORED, "0010") == 0) {
        strcat(newXOR, "2");
    } else if(strcmp(strXORED, "0011") == 0) {
        strcat(newXOR, "3");
    } else if(strcmp(strXORED, "0100") == 0) {
        strcat(newXOR, "4");
    } else if(strcmp(strXORED, "0101") == 0) {
        strcat(newXOR, "5");
    } else if(strcmp(strXORED, "0110") == 0) {
        strcat(newXOR, "6");
    } else if(strcmp(strXORED, "0111") == 0) {
        strcat(newXOR, "7");
    } else if(strcmp(strXORED, "1000") == 0) {
        strcat(newXOR, "8");
    } else if(strcmp(strXORED, "1001") == 0) {
        strcat(newXOR, "9");
    } else if(strcmp(strXORED, "1010") == 0) {
        strcat(newXOR, "A");
    } else if(strcmp(strXORED, "1011") == 0) {
        strcat(newXOR, "B");
    } else if(strcmp(strXORED, "1100") == 0) {
        strcat(newXOR, "C");
    } else if(strcmp(strXORED, "1101") == 0) {
        strcat(newXOR, "D");
    } else if(strcmp(strXORED, "1110") == 0) {
        strcat(newXOR, "E");
    } else if(strcmp(strXORED, "1111") == 0) {
        strcat(newXOR, "F");
    }
    return newXOR;
}


void setup() {
  RFduinoBLE.advertisementData = "echo";
//  RFduinoBLE.advertisementInterval = 500;
  RFduinoBLE.deviceName = myid;           // Specify BLE device name
  RFduinoBLE.txPowerLevel = -20;
  RFduinoBLE.begin();                            // Start the BLE stack
  Serial.begin(9600);                            // Debugging to the serial port
  Serial.print(myid);
  Serial.println(" device restarting...");
}

void RFduinoBLE_onConnect() {
  Serial.println("Start connection...");
  rssidisplay = true;
}

void RFduinoBLE_onDisconnect() {
  Serial.println("Disconnection...");
}

// Challenge Variables
char mychallenge[CHALLENGE_SIZE];
int lenofchallenge = 0;

// H(server_hash, challenge) Variables
SHA256 sha256_HMAC;
uint8_t resultsha[32];
char buf[HASH_SIZE*2+1];

void RFduinoBLE_onReceive(char *data, int len) {
  data[len] = 0;
  Serial.print("Received: ");
  Serial.print(data);
  Serial.println("...And sending back an echo with my ID.");

  strcat(mychallenge, data);
  lenofchallenge += strlen(data);


  if(lenofchallenge >= CHALLENGE_SIZE) {
    Serial.print("Message received: ");
    Serial.println(mychallenge);
    Serial.println(lenofchallenge);


    //Get H(K, challenge), Given Get K = H(H(password, salt)) given H(password, salt)
    sha256_HMAC.resetHMAC(mychallenge, CHALLENGE_SIZE);
    sha256_HMAC.update(testvector.server_data, strlen(testvector.server_data));
    sha256_HMAC.finalizeHMAC(mychallenge, CHALLENGE_SIZE, resultsha, HASH_SIZE);

    for(int i=0; i<sha256_HMAC.hashSize(); i++) {
      Serial.print(resultsha[i],HEX); Serial.print(" ");}
    Serial.print(" Result sha256_HMAC | Length: ");
    Serial.println(sha256_HMAC.hashSize());

    Serial.println("Test function");
    if (to_hex(buf, sizeof(buf), resultsha, sizeof(resultsha)))
      for(int i =0 ; i<HASH_SIZE*2; i++)
        Serial.print(buf[i]);
      Serial.println();

    Serial.println("Test XOR");

    char oneXOR[65];
    char twoXOR[65];
    char newXOR[65];

    strcpy(oneXOR, testvector.client_data);
    strcpy(twoXOR, buf);
    strcpy(newXOR, "");

    for(int i =0; i<64; i++){
      strcat(newXOR, xorHash(oneXOR[i], twoXOR[i]));
    }

    Serial.println();
    for(int i =0; i< 64; i++){
      Serial.print(newXOR[i]);
    }

    Serial.println();
    Serial.print(" Result xor2 | Length: ");
    Serial.println(sha256_HMAC.hashSize());

/*
 * These parts are for testing purposes
    // To transmit challenge to pinger
    int i=0;
    for(; i<lenofchallenge/20; i++) {
      RFduinoBLE.send((mychallenge+i*20), 20);
    }
    RFduinoBLE.send((mychallenge+i*20), lenofchallenge-i*20);

    //To transmit 64 char h(server_hash, challenge) to pinger
    i=0;
    for(; i<HASH_SIZE*2/20; i++) {
      RFduinoBLE.send((buf+i*20), 20);
    }
    RFduinoBLE.send((buf+i*20), HASH_SIZE*2-i*20);
*/

    //To transmit final data XOR(h(server_hash,challenge), client_hash)


    if (checkIFHexString(newXOR) == 0) {
      char x[65] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
      RFduinoBLE.send(x, strlen(x));
    } else {
      int i=0;
      for(; i<HASH_SIZE*2/20; i++) {
        RFduinoBLE.send((newXOR+i*20), 20);
      }
      RFduinoBLE.send((newXOR+i*20), HASH_SIZE*2-i*20);
    }

    
    memset(mychallenge, 0, lenofchallenge);
    lenofchallenge = 0;
  } //endif
}

void RFduinoBLE_onRSSI(int rssi) {
  if (rssidisplay) {
    Serial.print("RSSI is ");
    Serial.println(rssi);                        // print rssi value
    rssidisplay = false;
  }
}

int dotcount = 0;

void loop() {
  RFduino_ULPDelay(SECONDS(5));                // Ultra Low Power delay for 0.5 second (SECONDS(0.5), INFINITE)
  dotcount++;
  
  if (dotcount < 40) {
    Serial.print("-");
  } else {
    
    Serial.println();
    dotcount = 0;
  }
//   RFduinoBLE_onRSSI(1);
}

bool to_hex(char* dest, size_t dest_len, const uint8_t* values, size_t val_len) {
    static const char hex_table[] = "0123456789ABCDEF";
    if(dest_len < (val_len*2+1)) /* check that dest is large enough */
        return false;
    while(val_len--) {
        /* shift down the top nibble and pick a char from the hex_table */
        *dest++ = hex_table[*values >> 4];
        /* extract the bottom nibble and pick a char from the hex_table */
        *dest++ = hex_table[*values++ & 0xF];
    }
    *dest = 0;
    return true;
}
