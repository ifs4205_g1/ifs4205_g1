"""Backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.views import View
from User import views
from django.conf.urls import url,include


urlpatterns = [
    path('user/login/', views.login),
    path('', views.initial_page),
    # path('user/2fa/<str:hashed_username>', views.two_factor_authentication),
    path('user/register/', views.register),
    path('user/logout/', views.logout),
    path('user/2FA_setup/', views.get_token),
    path('user/password_reset', views.password_reset),
    path('reset/<str:user_id>', views.password_reset_confirm),
    path('user/password_reset/verify/', views.password_reset_verify),
]
