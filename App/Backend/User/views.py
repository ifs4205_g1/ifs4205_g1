import base64
import string
import random
from io import BytesIO
from smtplib import SMTPException

import pyotp
import traceback
from django.views.decorators.csrf import ensure_csrf_cookie

from django.contrib.auth.decorators import login_required
from qrcode import QRCode, constants
from django.contrib.auth.forms import PasswordResetForm
from django.shortcuts import render
from django.shortcuts import HttpResponseRedirect, HttpResponse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from User.models import User, UserInformation
from django.contrib.auth.tokens import default_token_generator
from django.template.loader import render_to_string

from django.core.mail import send_mail, BadHeaderError
from django.contrib.auth import logout as auth_logout
from TwoFA.models import TOTPDevice

import hashlib


def encrypt(pw):
    sha256 = hashlib.sha256()
    sha256.update(pw.encode())
    pw = sha256.hexdigest()
    return str(pw)


def initial_page(request):
    return render(request, "home.html")


@ensure_csrf_cookie
def register(request):
    if request.method == 'POST' and request.POST:
        data = request.POST
        username = data.get("username")
        nationality = data.get("nationality")
        contact_number = data.get("contact_number")
        password = data.get("password")
        email = data.get("email")
        idnum = data.get("id_num")
        countrycode = data.get("country_code")
        User.objects.create(
            username=username,
            password=encrypt(password),
        )
        exist = User.objects.filter(username=username).first()
        UserInformation.objects.create(
            pid=exist.pid,
            nationality=nationality,
            role="admin",
            email=email,
            username=username,
            idnum=idnum,
            countrycode=countrycode,
            contactnum=contact_number,
        )
        TOTPDevice.objects.create(
            pid=exist.pid,
            username=username,
            secret=encrypt(password).replace("9", str(random.randint(1, 7))).replace("8", str(random.randint(1, 7)))
        )
        response = HttpResponseRedirect('/user/2FA_setup/')
        response.set_cookie("officer_name", exist.username)
        return response
    return render(request, "user_register.html")


def get_token(request):
    if request.method == "GET":
        officer_name = request.COOKIES.get("officer_name")
        user_2fa = TOTPDevice.objects.filter(username=officer_name).first()
        qrcode = get_google_qrcode(user_2fa.secret_key, user_2fa.username)
        if not qrcode:
            return render(request, "user_register.html", {"register error: Cannot generate QR code!"})

        out = BytesIO()
        qrcode.save(out, 'PNG')
        pic_base64 = base64.b64encode(out.getvalue()).decode('ascii')
        return render(request, "user_get_token.html", {"qrcode": pic_base64})
    elif request.method == "POST":
        officer_name = request.COOKIES.get("officer_name")
        user_2fa = TOTPDevice.objects.filter(username=officer_name).first()
        user_input_token = request.POST.get("token_input")
        if not verify_google_code(user_2fa.secret_key, user_input_token):
            return render(request, "user_register.html", {"register error" + " token is not valid!"})
        else:
            response = HttpResponseRedirect("/user/login/")
            response.delete_cookie("officer_name")
            return response


@ensure_csrf_cookie
def login(request):
    if request.method == 'POST' and request.POST:
        # phone = request.POST.get("contact_number")
        username = request.POST.get("username")
        password = request.POST.get("password")
        # exist = User.objects.filter(contact_number=phone).first()
        exist = User.objects.filter(username=username).first()
        if exist:
            pid = exist.pid
            role = UserInformation.objects.filter(pid=pid).first()
            if role.role.lower() == "normal":
                info = UserInformation.objects.filter(pid=pid).first()
                encrypted_pw = encrypt(password)
                if password == exist.password:
                    hashed_username = encrypt(username)
                    # response = HttpResponseRedirect("/user/2fa/" + hashed_username + "/")
                    response = HttpResponseRedirect("/user/main/" + hashed_username + "/")
                    response.set_cookie("username", exist.username)

                    # qrcode = get_google_qrcode(info.token, User.username)
                    # if not qrcode:
                    #     return render(request, "login.html", {"login error: Cannot generate QR code!"})
                    #
                    # out = BytesIO()
                    # qrcode.save(out, 'PNG')
                    # pic_base64 = base64.b64encode(out.getvalue()).decode('ascii')
                    #
                    # response.headers["qrcode"] = pic_base64.encode('ascii')
                    return response
                else:
                    return render(request, 'login.html', {"login error": "password is invalid"})
            else:
                return render(request, 'login.html', {"login error": "User is not a valid user, is SHN officer or admin"})
        else:
            return render(request, 'login.html', {"login error": "Invalid user."})
    return render(request, "login.html")


def two_factor_authentication(request, user_hash):
    username = request.COOKIES.get('username')
    if encrypt(username) == user_hash:
        info = UserInformation.objects.filter(username=username).first()
        verification_code = request.POST.get("verification_code")
        if verification_code:
            if not verify_google_code(info.token, verification_code):
                return render(request, "login.html", {"login error" + " verification_code is not valid!"})
            else:
                return HttpResponseRedirect("user/main/" + user_hash + "/")
    else:
        return render(request, "2fa.html", {"login error" + " user is not valid!"})
    return render(request, "2fa.html")


def logout(request):
    # response = HttpResponseRedirect("/user/login/")
    # response.delete_cookie("username")
    # response.delete_cookie("csrftoken")
    # response.delete_cookie("officer_name")
    # return response
    auth_logout(request)
    return HttpResponseRedirect("/user/login")


def random_generate():
    length = 10
    random_number = ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))
    return random_number


@ensure_csrf_cookie
def password_reset(request):
    if request.method == "POST":
        username = request.POST.get("username")
        user = User.objects.filter(username=username).first()
        user_pid = user.pid
        info = UserInformation.objects.filter(pid=user_pid).first()
        email_entered = request.POST.get("email")
        count = User.objects.filter(username=username).count()
        if count == 1 and email_entered == info.email:
            email = info.email
            email_part = email[4:]
            # password = request.POST.get("password")
            # password = encrypt_pw(password)
            # User.objects.filter(username=username).update(password=password)

            # send email
            subject = "Password Reset Requested"
            verification_code = random_generate()
            response = HttpResponseRedirect("/user/password_reset/verify/")
            response.set_cookie("vrf_code", verification_code)
            response.set_cookie("admin_name", username)
            email_content = 'hello user ***' + email_part + ' your verification code is ' + verification_code

            try:
                send_mail(subject, email_content, 'zyl_19930309@163.com', [email], fail_silently=False)
            except SMTPException:
                return render(request, "forget_password.html", {"forget_password_tips": "fail to send email!"})

            return response
        return render(request, "forget_password.html", {"forget_password_tips": username + " is not exist!"})
    return render(request, "forget_password.html")


@ensure_csrf_cookie
def password_reset_verify(request):
    if request.method == "POST":
        input_code = request.POST.get("verification_code")
        verification_code = request.COOKIES.get("vrf_code")
        username = request.COOKIES.get("username")
        if input_code == verification_code:
            return HttpResponseRedirect("/reset/" + encrypt(username) + "/")
        else:
            return render(request, "forget_password_verify.html", {"forget_password_tips": "did not put the "
                                                                                             "right "
                                                                                             "verification "
                                                                                             "code!"})
    return render(request, "forget_password_verify.html")


@ensure_csrf_cookie
def password_reset_confirm(request, user_id):
    if request.method == "POST":
        username = request.POST.get("username")
        user = User.objects.filter(username=username).first()
        if encrypt(user.username) != user_id:
            return render(request, "forget_password_confirm.html", {
                "forget_password_tips": "user not exist"})

        if user:
            password = request.POST.get("password")

            User.objects.filter(username=username).update(password=password)
            response = HttpResponseRedirect("/user/login/")
            response.delete_cookie("username")
            response.delete_cookie("vrf_code")
            return response
        else:
            return render(request, "forget_password_confirm.html",
                          {"forget_password_tips": username + " is not exist!"})
    return render(request, "forget_password_confirm.html")


def verify_google_code(token, code):
    t = pyotp.totp.TOTP(token)
    result = t.verify(code)
    if result:
        return True
    else:
        return False


def get_google_qrcode(token, username):
    data = pyotp.totp.TOTP(token).provisioning_uri(username, issuer_name="IFS4205grp1")
    qr = QRCode(error_correction=constants.ERROR_CORRECT_L, version=1, box_size=6, border=4)
    try:
        qr.add_data(data)
        qr.make(fit=True)
        img = qr.make_image()
        return img
    except Exception as e:
        traceback.print_exc()
        return None

