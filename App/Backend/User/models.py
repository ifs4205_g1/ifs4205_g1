from django.db import models
import datetime
from datetime import date

class User(models.Model):
    pid = models.IntegerField(primary_key=True, serialize=True)
    username = models.CharField(max_length=25)
    password = models.CharField(max_length=32)
    # login_attempts = models.PositiveIntegerField()
    # login1 = models.DateTimeField(default=None, blank=True, null=True)
    # login2 = models.DateTimeField(default=None, blank=True, null=True)
    # login3 = models.DateTimeField(default=None, blank=True, null=True)

    #can add if needed
    class Meta:
        db_table = "credentials"


class UserInformation(models.Model):
    pid = models.IntegerField(primary_key=True, serialize=True)
    nationality = models.CharField(max_length=80)
    role = models.CharField(max_length=80)
    email = models.CharField(max_length=50, default="@")
    idnum = models.CharField(max_length=30)
    countrycode = models.CharField(max_length=2)
    contactnum = models.IntegerField()
    vaccinated = models.CharField(max_length=1)
    vaccinatedtime = models.DateTimeField("YYYY-MM-DD HH:MM:ss")
    latestcovidtest = models.CharField(max_length=8)

    class Meta:
        db_table = "person"

