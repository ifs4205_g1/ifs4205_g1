from django.db import models


# Create your models here.


class SHNId(models.Model):
    pid = models.IntegerField()
    qoid = models.IntegerField(primary_key=True)

    class Meta:
        db_table = "officials"


class Consent(models.Model):
    pid = models.IntegerField(primary_key=True)
    idflag = models.BooleanField()
    nameflag = models.BooleanField()
    nationalityflag = models.BooleanField()
    contactflag = models.BooleanField()
    addressflag = models.BooleanField()
    genderflag = models.BooleanField()

    class Meta:
        db_table = "consent"
