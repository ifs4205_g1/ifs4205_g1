from django.contrib import admin
from django.urls import path
from django.views import View
from SHNQuery import views


urlpatterns = [
    path('SHN_officer/main/<str:shn_hash>/', views.shn_query),
    path('SHN_officer/main/<str:shn_hash>/<str:user_hash>/update', views.update_swab_test_status),
    path('SHN_officer/main/<shn_hash>/<user_hash>/update/success', views.update_success)
]