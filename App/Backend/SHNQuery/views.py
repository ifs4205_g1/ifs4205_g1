from datetime import datetime

from django.shortcuts import render
from UserQuery.models import quarantineInfo
from User.models import UserInformation, User
from SHNQuery.models import SHNId, Consent
import hashlib
from django.shortcuts import HttpResponseRedirect, HttpResponse

# Create your views here.


def encrypt(pw):
    sha256 = hashlib.sha256()
    sha256.update(pw.encode())
    pw = sha256.hexdigest()
    return str(pw)


def shn_query(request, shn_hash):
    shn_manager = request.COOKIES.get('officer_name')
    if encrypt(shn_manager) == shn_hash:
        shn_officer = User.objects.filter(username=shn_manager).first()
        if shn_officer:
            officer_id = shn_officer.pid
            data = {}
            qoid_get = SHNId.objects.filter(pid=officer_id).first()
            qoid = qoid_get.qoid
            queries = quarantineInfo.objects.filter(qo_incharge=qoid).all()
            queries_list = list(queries)
            for query_of_patients in queries_list:
                patients_array = query_of_patients.patients
                for patient in patients_array:
                    consent = Consent.objects.filter(pid=patient).first()
                    user = User.objects.filter(pid=patient).first()
                    user_info = UserInformation.objects.filter(pid=patient).first()
                    quarantine = quarantineInfo.objects.filter(patients__contains=[patient]).first()
                    userdata = {}
                    idflag = consent.idflag
                    nameflag = consent.nameflag
                    nationalityflag = consent.nationalityflag
                    addressflag = consent.addressflag
                    contactflag = consent.contactflag

                    userdata["user_name"] = user.username if (nameflag) else "******"
                    userdata["id_number"] = user_info.idnum if (idflag) else "******"
                    userdata["nationality"] = user_info.nationality if (nationalityflag) else "******"
                    userdata["address"] = quarantine.qlocation if (addressflag) else "******"
                    userdata["contact"] = user_info.contactnum if (contactflag) else "******"


                    userdata["start_date"] = quarantine.start
                    userdata["end_date"] = quarantine.enddate,
                    userdata["QO_location"] = quarantine.qlocation

                    data[patient] = userdata
            return render(request, "main_page_officer.html", {'data': data})
    return render(request, "main_page_officer.html", {"error": "manager is not valid"})


def update_swab_test_status(request, shn_hash, user_hash):
    if request.method == 'POST':
        shn_manager = request.COOKIES.get('shn_username')
        user_name = request.POST.get("username")
        user_info = UserInformation.objects.filter(username=user_name).first()
        if encrypt(shn_manager) == shn_hash:
            if encrypt(user_name) == user_hash:
                if user_info:
                    swab = UserInformation.objects.filter(username=user_name).first().swab_test
                    swab_test = request.POST.get("swab_test_result")
                    time = datetime.now().strftime("%Y%m%d")
                    new_test = [swab_test, time]
                    swab.update(new_test)
                    UserInformation.objects.filter(username=user_name).update(swab_test=swab)
                    return HttpResponseRedirect("/shnofficer/main/" + shn_hash + "/" + user_hash + "/update/success/")
    return render(request, "update_vaccinated_status.html")


def update_success(request, shn_hash, user_hash):
    return render(request, "update_success.html")
