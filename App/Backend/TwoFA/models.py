from django.db import models
import binascii, time
from django.conf import settings
from pyotp import totp
# Create your models here.


class TOTPDevice(models.Model):
    pid = models.IntegerField(primary_key=True)
    username = models.CharField(default="", max_length=20)
    secret_key = models.CharField(default="123jklmnbvfcdxz", max_length=40)

    class Meta:
        db_table = "TOTPDevice"

