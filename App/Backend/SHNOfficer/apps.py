from django.apps import AppConfig


class ShnofficerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'SHNOfficer'
