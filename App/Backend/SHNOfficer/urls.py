"""Backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.views import View
from SHNOfficer import views
from django.conf.urls import url,include


urlpatterns = [
    path('SHN_officer/login/', views.login),
    # path('SHN_officer/2fa/<str:hashed_officer_name>/', views.two_factor_authentication),
    path('SHN_officer/register/', views.register),
    path('SHN_officer/logout/', views.logout),
    path('SHN_officer/2FA_setup/', views.get_token),
    path('SHN_officer/password_reset/', views.password_reset),
    path('reset/<str:officer_id>/', views.password_reset_confirm),
    path('SHN_officer/password_reset/verify/', views.password_reset_verify),
    path(r'', include('SHNQuery.urls')),
]
