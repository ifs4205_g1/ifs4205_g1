from django.contrib import admin
from django.urls import path
from django.views import View
from AdminQuery import views


urlpatterns = [
    path('admin_officer/main/<str:admin_hash>/', views.main_page),
    # path('admin_officer/main/<shn_hash>/<user_hash>/update', views.update_swab_test_status),
    # path('admin_officer/main/<shn_hash>/<user_hash>/update/success', views.update_success)
]