from django.shortcuts import render


# Create your views here.
def main_page(request, admin_hash):
    return render(request, "admin_main_page.html")
