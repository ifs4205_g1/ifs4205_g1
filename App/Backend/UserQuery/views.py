from django.shortcuts import render
from User.models import UserInformation, User
import hashlib
from django.views.decorators.csrf import ensure_csrf_cookie
from UserQuery.models import quarantineInfo


def encrypt(pw):
    sha256 = hashlib.sha256()
    sha256.update(pw.encode())
    pw = sha256.hexdigest()
    return str(pw)


@ensure_csrf_cookie
# Create your views here.
def query(request, user_hash):
    username = request.COOKIES.get("username")
    get_pid = User.objects.filter(username=username).first()
    pid = get_pid.pid
    user_info = UserInformation.objects.filter(pid=pid).first()
    if encrypt(username) != user_hash:
        return render(request, "user_main_page.html", {"query": "the user name is incorrect"})
    if user_info:
        contact_num = user_info.contactnum
        vaccinated = user_info.vaccinated
        vaccinated_time_stamp = user_info.vaccinatedtime
        latest_covid_test = user_info.latestcovidtest
        country_code = user_info.countrycode
        pid = user_info.pid
        data = {
            "username": username,
            "contact_number": contact_num,
            "country_code": country_code,
            "vaccinated": vaccinated,
            "vaccinated_time_stamp": vaccinated_time_stamp,
            "latest_covid_test": latest_covid_test
        }
        quarantine = quarantineInfo.objects.filter(patients__contains=[pid]).first()
        if quarantine:
            start_date = quarantine.start
            end_date = quarantine.enddate
            QO_location = quarantine.qlocation

            data["start_date"] = start_date,
            data["end_date"] = end_date,
            data["QO_location"] = QO_location
            return render(request, "user_main_page.html", data)
        else:
            data["error"] = "user has no quarantine information"
            return render(request, "user_main_page.html", data)




