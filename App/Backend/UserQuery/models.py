from django.db import models
from django.contrib.postgres.fields import ArrayField


# Create your models here.
# class UserInfo(models.Model):
#     pid = models.IntegerField()
#     genre = models.CharField(max_length=5)
#     identification = models.CharField(max_length=255)
#     item = models.CharField(max_length=10)
#     country_code = models.IntegerField()
#     username = models.CharField(max_length=25)
#     contact_number = models.PositiveIntegerField()
#     bleId = models.IntegerField()
#     vaccinated = models.CharField(max_length=1)
#     vaccinated_time_stamp = models.DateTimeField("YYYY-MM-DD")
#     covid_status = models.CharField(max_length=1)
#     email = models.CharField(max_length=40, default="@")
#     # token = models.CharField(null=False, max_length=255)
#     #swab_test = models.SET()
#
#     class Meta:
#         db_table = "persondetails"


class quarantineInfo(models.Model):
    qid = models.IntegerField(serialize=True, primary_key=True)
    qlocation = models.CharField(max_length=255)
    start = models.DateTimeField("YYYY-MM-DD HH:MM:ss", default='9999-12-31 00:00:00')
    enddate = models.DateTimeField("YYYY-MM-DD HH:MM:ss", default='9999-12-31 00:00:00')
    qo_incharge = models.IntegerField(default=-1)
    numpersons = models.IntegerField(default=2)
    patients = ArrayField(models.IntegerField(), blank=False, default=list)

    class Meta:
        db_table = "quarantine"
