from django.apps import AppConfig


class AdminofficerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'AdminOfficer'
