import Login from '../components/Login.vue'
import HomePage from '../components/HomePage'
import MainUserPage from "../components/MainUserPage";
import MainAdminPage from "../components/MainAdminPage";
import MainOfficialPage from "../components/MainOfficialPage";
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    routes: [
        {path: '/', component: HomePage},
        {path: '/login', component: Login},
        {path: '/main-user', component: MainUserPage},
        {path: '/main-admin', component: MainAdminPage},
        {path: '/main-official', component: MainOfficialPage}
    ]
})
